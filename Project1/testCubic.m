function testCubic()

    expectedRoots = [
        1, 2, 3;
        1, 10, 10;
        1, 100, 1000;
        -2, 2, 5;
        1j, -1j, 6;
        2 + 1j, 2 - 1j, 43;
    ];
    tolerance = 1e-6;

    numPolynomials = size(expectedRoots, 1);
    failures = 0;
    for index = 1:numPolynomials
        r1 = expectedRoots(index, 1);
        r2 = expectedRoots(index, 2);
        r3 = expectedRoots(index, 3);
        % NOTE: We assume r3 is the largest expected.
        [a, b, c, d] = rootsToCoeffs(r1, r2, r3);
        [largestRoot] = cubic(a, b, c, d);
        % Compare the actual and expected.
        maxRelErr = compareRoots(r3, largestRoot);
        if maxRelErr > tolerance
            failures = failures + 1;
            disp('Failed to find roots to precision');
            disp('Expected:');
            disp(r3);
            disp('Actually computed:');
            disp(largestRoot);
        end
    end
    if failures == 0
        disp('Success');
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helpers for root comparison %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [largestRoot] = cubic(a, b, c, d)
    roots = [];
    coeffs = [a b c d];
    for i = 1:3
        initialGuess = 100000;
        newGuess = 0;
        derivitive = polyder(coeffs);
        diff = 1;
        while (abs(diff) > 10^-6)
            newGuess = initialGuess - polyval(coeffs, initialGuess)/polyval(derivitive, initialGuess);
            diff = initialGuess - newGuess;
            initialGuess = newGuess;
        end
        roots = [roots newGuess];
    end
    p = [1, -newGuess];
    coeffs = deconv(coeffs, p);
    largestRoot = max(roots);
end

function [a, b, c, d] = rootsToCoeffs(r1, r2, r3)
    a = 1;
    b = -(r1 + r2 + r3);
    c = r1 * r2 + r2 * r3 + r3 * r1;
    d = -(r1 * r2 * r3);
end

function maxRelErr = compareRoots(expected, actual)
    maxRelErr = -Inf;
    if expected == 0
        if actual ~= 0
            % If we wanted 0 but didn't have it, infinite error.
            maxRelErr = Inf;
        end
    else
        relErr = abs(expected - actual) / abs(expected);
        % Make sure error of 0.0 isn't a bad thing.
        if relErr ~= 0
            maxRelErr = relErr;
        end
    end
end