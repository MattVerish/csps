function [points] = makePlot()
    tolerance = 1e-5;
    % We want 20 radii
    radii = linspace(0, 10, 20 + 1);
    radii = radii(2:end);
    % We use 50 points for each radius, 50 * 20 == 1000.
    thetaVals = linspace(0, 2 * pi, 50 + 1);
    thetaVals = thetaVals(1:end - 1);
    % Hack to make sure we get the negative real axis
    thetaVals(26) = pi;

    index = 1;
    % points has x, y as the first 2 columns and the color as third.
    points = zeros(1000, 3);
    for radius = radii
        for theta = thetaVals
           % Hack to make sure we get the negative real axis
            if theta == pi
                z = -radius;
            else
                z = radius * exp(1j * theta);
            end
            x = real(z);
            y = imag(z);
            limit = imaginaryUnits(x, y);
            if abs(limit - i) < tolerance
                color = 0;
            elseif abs(limit + i) < tolerance
                color = 1;
            else
                color = 2;
            end
            points(index, 1) = x;
            points(index, 2) = y;
            points(index, 3) = color;
            index = index + 1;
        end
    end
    scatter(points(:, 1), points(:, 2), [], points(:, 3));
    axis('equal');
end