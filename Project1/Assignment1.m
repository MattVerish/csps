function [largestRoot] = cubic(a, b, c, d)
    roots = []
    coeffs = [a b c d];
    for i = 1:3
        initialGuess = 100000;
        newGuess = 0;
        derivitive = polyder(coeffs);
        diff = 1;
        while (abs(diff) > 10^-6)
            newGuess = initialGuess - polyval(coeffs, initialGuess)/polyval(derivitive, initialGuess);
            diff = initialGuess - newGuess;
            initialGuess = newGuess;
        end
        roots = [roots newGuess];
    end
    p = [1, -newGuess]
    coeffs = deconv(coeffs, p)
    largestRoot = max(roots)
end

function [limit] = imaginaryUnits(x0, y0)
end
    